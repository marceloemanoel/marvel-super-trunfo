function embaralhar(cartas = []) {
  const cartasEmbaralhadas = cartas.slice(0);
  let indice = cartasEmbaralhadas.length;

  while (indice !== 0) {
    const novaPosicao = Math.floor(Math.random() * indice);
    indice -= 1;

    const carta = cartasEmbaralhadas[indice];
    cartasEmbaralhadas[indice] = cartasEmbaralhadas[novaPosicao];
    cartasEmbaralhadas[novaPosicao] = carta;
  }

  return cartasEmbaralhadas;
}

function distribuir(numeroDeJogadores = 2, cartas = []) {
  const qtdDeCartasPorJogador = Math.floor(cartas.length / numeroDeJogadores);
  const cartasADistribuir = cartas.slice(0, qtdDeCartasPorJogador * numeroDeJogadores);
  const sobras = cartas.slice(cartasADistribuir.length);

  const cartasNaMao = [];

  while (cartasADistribuir.length > 0) {
    for (let jogador = 0; jogador < numeroDeJogadores; jogador++) {
      if (!cartasNaMao[jogador]) {
        cartasNaMao[jogador] = [];
      }

      cartasNaMao[jogador].push(cartasADistribuir.shift());
    }
  }

  return { sobras, cartasNaMao };
}

function criarCarta({ personagem, frente } = {}) {
  const personagemCarta = () => `
    <img class="carta__personagem__shadow" src="${personagem.img}" alt="${personagem.personagem}" />
    <img class="carta__personagem__clip" src="${personagem.img}" alt="${personagem.personagem}" />
    <span class="carta__nome">${personagem.personagem}</span>
    <div class="carta__categorias">
      <div class="carta__categoria">
        <span class="carta__categoria__nome">Força</span>
        <span class="carta__categoria__valor">${personagem.forca}</span>
      </div>
      <div class="carta__categoria">
        <span class="carta__categoria__nome">Velocidade</span>
        <span class="carta__categoria__valor">${personagem.velocidade}</span>
      </div>
      <div class="carta__categoria">
        <span class="carta__categoria__nome">Habilidade</span>
        <span class="carta__categoria__valor">${personagem.habilidade}</span>
      </div>
      <div class="carta__categoria">
        <span class="carta__categoria__nome">Equipamentos</span>
        <span class="carta__categoria__valor">${personagem.equipamento}</span>
      </div>
      <div class="carta__categoria">
        <span class="carta__categoria__nome">Inteligência</span>
        <span class="carta__categoria__valor">${personagem.inteligencia}</span>
      </div>
    </div>
  `;

  const carta = document.createElement("div");
  carta.classList.add("carta");
  carta.classList.add(frente ? "carta__frente" : "carta__verso");
  carta.innerHTML = personagem
    ? personagemCarta()
    : `<img class="carta__img-fundo" src="./assets/img/bg-carta.jpg" alt="carta virada" />`;
  return carta;
}
