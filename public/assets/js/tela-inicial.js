function telaInicial(jogo) {
  const telaInicial = document.createElement("section");
  telaInicial.className = "tela-inicial";
  telaInicial.innerHTML = `
    <header class="tela-inicial__header">
      <span class="tela-inicial__header__a">A</span>
      <span class="tela-inicial__header__vengers">vengers</span>
      <span class="tela-inicial__header__assemble">assemble!</span>
    </header>
    <div class="tela-inicial__input">
      <label for="numero-de-jogadores">Nº de Jogadores</label>
      <input id="numero-de-jogadores" min="2" max="8" step="1" value="2" type="number" />
    </div>
    <button>Distribuir Cartas</button>
  `;

  const jogadores = telaInicial.querySelector("input[type=number]");
  const botaoDistribuirCartas = telaInicial.querySelector("button");

  const atualizaNumeroDeJogadores = event => {
    const min = Number.parseInt(jogadores.min, 10);
    const max = Number.parseInt(jogadores.max, 10);
    const value = Number.parseInt(jogadores.value, 10);

    if (event.deltaY > 0 && value < max) {
      jogadores.value = value + 1;
    }

    if (event.deltaY < 0 && value > min) {
      jogadores.value = value - 1;
    }
  };

  jogadores.addEventListener("wheel", atualizaNumeroDeJogadores, { passive: true });

  botaoDistribuirCartas.addEventListener("click", () => {
    jogo.jogadores = Number.parseInt(jogadores.value, 10);
    jogo.proximaEtapa();
  });

  return telaInicial;
}
