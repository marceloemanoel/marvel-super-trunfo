const cartas = [
  {
    id: "a1",
    personagem: "Homem de Ferro",
    img: "./assets/img/iron-man.png",
    forca: 5,
    velocidade: 6,
    habilidade: 2,
    equipamento: 5,
    inteligencia: 6
  },
  {
    id: "a2",
    personagem: "Capitão América",
    img: "./assets/img/capitao-america.png",
    forca: 3,
    velocidade: 2,
    habilidade: 6,
    equipamento: 2,
    inteligencia: 3
  },
  {
    id: "a3",
    personagem: "Thor",
    img: "./assets/img/thor.png",
    forca: 6,
    velocidade: 5,
    habilidade: 3,
    equipamento: 3,
    inteligencia: 2
  },
  {
    id: "a4",
    personagem: "Hulk",
    img: "./assets/img/hulk.png",
    forca: 6,
    velocidade: 5,
    habilidade: 4,
    equipamento: 1,
    inteligencia: 1
  },
  {
    id: "a5",
    personagem: "Nick Fury",
    img: "./assets/img/nick-fury.png",
    forca: 1,
    velocidade: 2,
    habilidade: 4,
    equipamento: 6,
    inteligencia: 3
  },
  {
    id: "a6",
    personagem: "Viuva Negra",
    img: "./assets/img/viuva-negra.png",
    forca: 1,
    velocidade: 2,
    habilidade: 6,
    equipamento: 4,
    inteligencia: 3
  },
  {
    id: "b1",
    personagem: "Gavião Arqueiro",
    img: "./assets/img/gaviao-arqueiro.png",
    forca: 1,
    velocidade: 1,
    habilidade: 6,
    equipamento: 5,
    inteligencia: 2
  },
  {
    id: "b2",
    personagem: "Agente Hill",
    img: "./assets/img/maria-hill.png",
    forca: 1,
    velocidade: 1,
    habilidade: 4,
    equipamento: 5,
    inteligencia: 3
  },
  {
    id: "b3",
    personagem: "Agente Coulson",
    img: "./assets/img/phil-coulson.png",
    forca: 1,
    velocidade: 1,
    habilidade: 3,
    equipamento: 5,
    inteligencia: 2
  },
  {
    id: "b4",
    personagem: "Tony Start",
    img: "./assets/img/tony-stark.png",
    forca: 1,
    velocidade: 1,
    habilidade: 1,
    equipamento: 5,
    inteligencia: 6
  },
  {
    id: "b5",
    personagem: "Bruce Banner",
    img: "./assets/img/bruce-banner.png",
    forca: 1,
    velocidade: 1,
    habilidade: 1,
    equipamento: 3,
    inteligencia: 6
  },
  {
    id: "b6",
    personagem: "Loki",
    img: "./assets/img/loki.png",
    forca: 5,
    velocidade: 4,
    habilidade: 4,
    equipamento: 5,
    inteligencia: 5
  },
  {
    id: "c1",
    personagem: "Chitauri",
    img: "./assets/img/chitauri.png",
    forca: 2,
    velocidade: 1,
    habilidade: 2,
    equipamento: 3,
    inteligencia: 1
  },
  {
    id: "c2",
    personagem: "Leviathan",
    img: "./assets/img/leviathan.png",
    forca: 5,
    velocidade: 5,
    habilidade: 1,
    equipamento: 5,
    inteligencia: 1
  },
  {
    id: "c3",
    personagem: "Caveira Vermelha",
    img: "./assets/img/red-skull.png",
    forca: 3,
    velocidade: 2,
    habilidade: 4,
    equipamento: 4,
    inteligencia: 3
  },
  {
    id: "c4",
    personagem: "Soldado da Hidra",
    img: "./assets/img/soldado-hydra.png",
    forca: 2,
    velocidade: 1,
    habilidade: 2,
    equipamento: 4,
    inteligencia: 2
  },
  {
    id: "c5",
    personagem: "Chicote Negro",
    img: "./assets/img/chicote-negro.png",
    forca: 2,
    velocidade: 2,
    habilidade: 3,
    equipamento: 4,
    inteligencia: 6
  },
  {
    id: "c6",
    personagem: "Monge de Ferro",
    img: "./assets/img/monge-de-ferro.png",
    forca: 5,
    velocidade: 4,
    habilidade: 2,
    equipamento: 5,
    inteligencia: 4
  },
  {
    id: "D1",
    personagem: "Gigante de Gelo",
    img: "./assets/img/gigante-de-gelo.png",
    forca: 4,
    velocidade: 3,
    habilidade: 3,
    equipamento: 2,
    inteligencia: 2
  },
  {
    id: "D2",
    personagem: "Abominável",
    img: "./assets/img/abominacao.png",
    forca: 6,
    velocidade: 3,
    habilidade: 3,
    equipamento: 1,
    inteligencia: 3
  },
  {
    id: "D3",
    personagem: "HeimDall",
    img: "./assets/img/heimdall.png",
    forca: 4,
    velocidade: 3,
    habilidade: 3,
    equipamento: 3,
    inteligencia: 2
  },
  {
    id: "D4",
    personagem: "Emil Blonsky",
    img: "./assets/img/emil-blonsky.png",
    forca: 2,
    velocidade: 3,
    habilidade: 5,
    equipamento: 2,
    inteligencia: 3
  },
  {
    id: "D5",
    personagem: "Howard Stark",
    img: "./assets/img/howard-stark.png",
    forca: 1,
    velocidade: 1,
    habilidade: 2,
    equipamento: 4,
    inteligencia: 6
  },
  {
    id: "D6",
    personagem: "Máquina de Combate",
    img: "./assets/img/maquina-de-combate.png",
    forca: 5,
    velocidade: 5,
    habilidade: 2,
    equipamento: 5,
    inteligencia: 3
  },
  {
    id: "E1",
    personagem: "Homem de Ferro Mark V",
    forca: 5,
    velocidade: 4,
    habilidade: 2,
    equipamento: 4,
    inteligencia: 6
  },
  {
    id: "E2",
    personagem: "Odin",
    img: "./assets/img/odin.png",
    forca: 5,
    velocidade: 2,
    habilidade: 1,
    equipamento: 6,
    inteligencia: 6
  },
  {
    id: "E3",
    personagem: "Valquíria",
    img: "./assets/img/valquiria.png",
    forca: 4,
    velocidade: 2,
    habilidade: 4,
    equipamento: 3,
    inteligencia: 3
  },
  {
    id: "E4",
    personagem: "Dum Dum Dugan",
    img: "./assets/img/dum-dum-dugan.png",
    forca: 1,
    velocidade: 2,
    habilidade: 3,
    equipamento: 2,
    inteligencia: 2
  },
  {
    id: "E5",
    personagem: "Soldado Invernal",
    img: "./assets/img/soldado-invernal.png",
    forca: 1,
    velocidade: 2,
    habilidade: 3,
    equipamento: 3,
    inteligencia: 3
  },
  {
    id: "E6",
    personagem: "Destruidor",
    img: "./assets/img/destruidor.png",
    forca: 6,
    velocidade: 4,
    habilidade: 3,
    equipamento: 3,
    inteligencia: 1
  }
];
