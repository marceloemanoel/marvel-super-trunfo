// Inicializa a tela inicial assim que a página é carregada.
window.addEventListener("load", superTrunfo);

function superTrunfo() {
  const etapas = [telaInicial, jogar];
  let etapaAtual = undefined;
  let indiceAtual = -1;

  const jogo = {
    iniciar: () => {
      if (etapaAtual) {
        document.body.removeChild(etapaAtual);
      }

      etapaAtual = undefined;
      indiceAtual = -1;

      jogo.proximaEtapa();
    },
    proximaEtapa: () => {
      if (etapaAtual) {
        document.body.removeChild(etapaAtual);
      }

      indiceAtual = Math.min(indiceAtual + 1, etapas.length);
      etapaAtual = etapas[indiceAtual](jogo);

      document.body.appendChild(etapaAtual);
    },
    etapaAnterior: () => {
      if (etapaAtual) {
        document.body.removeChild(etapaAtual);
      }

      indiceAtual = Math.max(0, indiceAtual - 1);
      etapaAtual = etapas[indiceAtual](jogo);

      document.body.appendChild(etapaAtual);
    }
  };

  jogo.iniciar();
}
