function jogar(jogo) {
  function montarSobras(sobras) {
    return sobras.map((_, indice) => {
      const posicao = `${sobras.length - indice * 5}px`;

      const cartaElement = criarCarta();
      cartaElement.style.zIndex = indice;
      cartaElement.style.top = posicao;
      cartaElement.style.left = posicao;

      return cartaElement;
    });
  }

  function montarMaos(maos) {
    let cartaSelecionada = undefined;

    const levantarCarta = carta => {
      if (carta !== cartaSelecionada) {
        retornarCartaSelecionada();

        cartaSelecionada = carta;
        cartaSelecionada.classList.add("levantar");
      }
    };

    const retornarCartaSelecionada = () => {
      if (cartaSelecionada) {
        cartaSelecionada.classList.remove("levantar");
        cartaSelecionada.classList.add("abaixar");
        const cartaRetornada = cartaSelecionada;
        window.setTimeout(() => {
          cartaRetornada.classList.remove("abaixar");
        }, 500);
        cartaSelecionada = undefined;
      }
    };

    document.addEventListener("click", retornarCartaSelecionada);

    return maos.slice(0, 1).map((cartas, jogador) => {
      const mao = document.createElement("ul");
      mao.className = `jogador-${jogador}`;
      mao.style.width = `${cartas.length * 3}em`;

      // De onde saiu essa equação da rotação inicial?
      // -45º foi um valor escolhido
      // daí pra frente é uma regra de três simples.
      //
      // 15 cartas -> -45º
      //  x cartas -> rotacaoInicial?
      //
      // 15 * rotacaoInicial = -45 * x
      // rotacaoInicial = (-45 * x) / 15
      const rotacaoInicial = (-45 * cartas.length) / 15;

      cartas.forEach((c, indice) => {
        const rotacao = rotacaoInicial + 6 * indice;

        const li = document.createElement("li");
        li.style.left = `${indice * 2}em`;
        li.style.transformOrigin = "bottom-right";
        li.style.transform = `rotate(${rotacao}deg)`;

        const cartaElement = criarCarta({ personagem: c, frente: true });
        cartaElement.addEventListener("mouseover", () => levantarCarta(cartaElement));

        li.appendChild(cartaElement);
        mao.appendChild(li);
      });

      return mao;
    });
  }

  const { sobras, cartasNaMao } = distribuir(jogo.jogadores, embaralhar(cartas.slice()));
  const mesa = document.createElement("section");
  mesa.className = "jogo";
  mesa.innerHTML = `
    <section id="sobras" class="monte-de-cartas">
      <div class="cartas"></div>
    </section>
  `;
  const cartasNoMonte = document.querySelector("#sobras > .cartas");
  montarSobras(sobras).forEach(carta => cartasNoMonte.appendChild(carta));
  montarMaos(cartasNaMao).forEach(mao => mesa.appendChild(mao));

  return mesa;
}
